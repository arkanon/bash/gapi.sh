#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2019/04/12 (Fri) 14:24:46 -03
# 2019/04/12 (Fri) 08:37:27 -03
# 2019/04/12 (Fri) 04:27:31 -03
# 2019/04/11 (Thu) 14:02:04 -03
# 2019/04/09 (Tue) 15:13:54 -03
# 2019/04/08 (Mon) 15:55:52 -03
# 2019/04/04 (Thu) 14:58:27 -03
# 2019/04/03 (Wed) 14:03:22 -03
# 2019/04/02 (Tue) 13:59:55 -03
# 2019/04/01 (Mon) 13:53:38 -03
# 2019/03/29 (Fri) 15:24:25 -03
# 2019/03/28 (Thu) 14:13:24 -03
# 2019/03/27 (Wed) 14:22:05 -03
# 2019/03/17 (Sun) 22:06:22 -03
# 2019/03/15 (Fri) 15:16:36 -03
# 2019/03/15 (Fri) 02:12:08 -03
# 2019/03/14 (Thu) 02:17:15 -03
# 2019/03/10 (Sun) 17:09:35 -03
# 2019/03/01 (Fri) 14:17:17 -03
# 2019/03/01 (Fri) 02:21:45 -03
# 2019/02/27 (Wed) 14:14:11 -03
# 2019/02/26 (Tue) 14:54:00 -03
# 2019/02/18 (Mon) 14:14:21 -03
# 2019/02/16 (Sat) 23:29:11 -03
# 2019/02/15 (Fri) 14:13:53 -02
# 2019/02/11 (Mon) 15:38:09 -02
# 2019/02/10 (Sun) 20:56:07 -02
# 2019/02/08 (Fri) 14:45:52 -02
# 2019/02/08 (Fri) 00:06:37 -02
# 2019/02/07 (Thu) 10:08:23 -02
# 2019/02/06 (Wed) 23:57:49 -02
# 2019/02/06 (Wed) 14:53:13 -02
# 2019/02/05 (Tue) 23:43:11 -02
# 2019/02/05 (Tue) 20:52:40 -02
#
#   git add . ; git commit -m "$msg" ; git push
#
# TODO
# ok cache dos dados do cliente
# ok detectar token inválido
# ok mkdir na raiz não usa o nome definido
# ok parâmetro gapi para apenas setar cliente
# ok implementar lwd
# ok incluir no prompt o valor de $gPWD
# ok gLrealpath interferindo com a função lwd (sessão shell do tty retorna com pwd=/): necessário usar 'builtin cd'
# ok resp3 e resp4 identando a cada source de gapi.sh (saida de status sendo armazenada com identação em $gapist)
# ok extrair do git este arquivo e seu hitórico para um repositório especifico <http://stackoverflow.com/a/43004729>
# ok opção para remover a biblioteca
# ok gcd aceitar -
# ok filtrar ações e funções da biblioteca automaticamente para o help
# ok traduzir descrições das funções
# ok adicionar . e .. na sada do gls
# -- caracteres de controle do prompt se perdendo com a posição do cursor na linha
# -- gls mostrando alguns arquivos (ex: z2) duas vezes
# -- configurar autocompletar do bash para diretórios e arquivos
# -- generalizar procedimento comum às funções que precisam verificar existência e id de elementos do path
# -- detectar token expirado
# -- auto-source da biblioteca se houverem alterações
# -- gmkfile com formatação de margin, orientação, fonte e tamanho
# -- gupdate manter formatação do documento
# -- não deletar/renomear diretório se estiver no pwd
# -- listar lixeira
# -- cp/mv/rm aceitar lista de elementos

# >> Google Drive REST API <<
#
#   <http://guidingtech.com/google-drive-storage-guide-what-counts-what-doesnt>
#   <http://developers.google.com/api-client-library/javascript/start/start-js>
#   <http://developers.google.com/drive/api/v3/mime-types>
#   <http://developers.google.com/drive/api/v3/multipart-upload>
#   <http://developers.google.com/drive/api/v3/reference/files/create>
#   <http://developers.google.com/drive/api/v3/reference/files/emptyTrash>
#   <http://developers.google.com/drive/api/v3/reference/files/list>
#   <http://developers.google.com/drive/api/v3/reference/files>
#   <http://developers.google.com/drive/api/v3/reference/permissions/create>
#   <http://developers.google.com/drive/api/v3/reference/permissions>
#   <http://developers.google.com/drive/api/v3/search-parameters>



____INT_LIB()(:) # >>------------------------------------------------------------------------------<< #



  # <http://newfivefour.com/unix-urlencode-urldecode-command-line-bash.html>

  urlencode()
  {
    for (( i=0; i<"${#1}"; i++ ))
    {
      local c="${1:i:1}"
      case $c in
        [a-zA-Z0-9.~_-]) printf "$c" ;;
        *) printf '%s' "$c" | xxd -p -c1 | while read c; do printf '%%%s' "$c"; done
      esac
    }
    echo
  }

  urldecode()
  {
    local url_encoded="${1//+/ }"
    printf %b "${url_encoded//%/\\x}"
  }



  column0()
  {
    man -P cat column | grep -qE '^ +-n ' && local nomer='-n'
    man -P cat column | grep -qE '^ +-e ' && local empty='-e'
    LC_ALL=pt_BR.UTF-8 column $empty $nomer -ts ${1:-$'\t'}
  }



  ac()
  {

    # <http://misc.flogisoft.com/bash/tip_colors_and_formatting>
    #
    # *  3 dark  [F]oreground
    # *  4 dark  [B]ackground
    # *  9 light [F]oreground
    # * 10 light [B]ackground
    #
    #   [f]ormatting     [C]olor
    # * 0 reset        * 0 black
    #   1 bold/bright  * 1 red
    # * 2 dim          * 2 green
    #   3 italic       * 3 yellow
    # * 4 underline    * 4 blue
    # * 5 blink        * 5 magenta
    #   6 overline     * 6 cyan
    #   7 reverse      * 7 white
    #   8 hidden
    #   9 strikeout
    #
    #   echo "^[[f;FC;BCm"

    typeset -A clnam clcod fglev bgnam bglev blnam blsty
    clnam=( [k]=black] [r]=r]ed  [g]=g]reen [y]=y]ellow [b]=b]lue [m]=m]agenta [c]=c]yan [w]=w]hite )
    clcod=( [k]=0      [r]=1     [g]=2      [y]=3       [b]=4     [m]=5        [c]=6     [w]=7      )
    fglev=( [1]=0\;9   [2]=2\;9  [3]=0\;3   [4]=2\;3 )
    bgnam=( [l]=l]ight [d]=d]ark )
    bglev=( [l]=10     [d]=4     )

    local fc fl bc bl u b i st tx f b t1 t2 cod sed l c # e off

      e=$'\e'
    off="$e[0m"
     tx=$([[ $# == 1 ]] && echo " text " || echo "$2")
      l="b c m r g y w k"
      c=$(set | grep -E "[${l// /}][1-4][${l// /}][dl][ub]{0,2}=")

    case $* in
    e) for f in $l
       {
         t1=
         for b in $l
         {
           t2=$($FUNCNAME $f$b)
           t1=$(paste -d "" <(echo "$t1") <(echo "$t2"))
         }
         echo "$t1"
       }
       echo
       return
       ;;
    s) for f in $l
       {
         for b in $l
         {
           $FUNCNAME $f$b ""
         }
       }
       return
       ;;
    u) unset $(cut -d= -f1 <<< $c) c
       return
       ;;
    l) echo
       echo    "$c"
       echo
       echo -n "$c" | wc -l
       echo
       return
       ;;
    esac

    case ${#1} in

    4|5|6) # color ansi text
           fc=${clcod[${1:0:1}]}
           fl=${fglev[${1:1:1}]}
           bc=${clcod[${1:2:1}]}
           bl=${bglev[${1:3:1}]}
           for i in 4 5
           {
             case ${1:$i:1} in
             u) st[$i]=\;4 ;;
             b) st[$i]=\;5 ;;
             *) st[$i]=
             esac
           }
           cod="$e[$fl$fc;$bl$bc${st[4]}${st[5]}m"
           echo -en "$cod$tx$off"
           eval $1=\"$cod\"
           ;;

    2    ) # demo grid
           fc=${1:0:1}
           bc=${1:1:1}
           [[ $tx ]] && echo
           for b in '' b
           {
             for u in '' u
             {
               for fl in ${!fglev[*]}
               {
                 for bl in l d
                 {
                   [[ $tx ]] && echo -n '  ' ; $FUNCNAME $fc$fl$bc$bl$u$b "$tx"
                 }
                 [[ $tx ]] && echo
               }
               [[ $tx ]] && echo
             }
           }
           ;;

    *    )
           sed="s/(.)\]/$e[1;33m\1$off/g"
           echo
           $FUNCNAME y1kd "ANSI Color Abstracion" ; echo
           echo
           $FUNCNAME w1kd Usage: ; echo " $FUNCNAME e|s|u|l | <fc><fl><bc><bl>[u][b]|<fc><bc> [text]"
           echo
           echo "        e   tabela com exemplo de combinações"
           echo "        s   seta todas as variáveis"
           echo "        u   apaga todas as variáveis"
           echo "        l   seta todas as variáveis"
           echo
           echo "       fc   foreground color"
           echo "       fl   foreground level"
           echo "       bc   background color"
           echo "       bl   background level"
           echo "        u   underline"
           echo "        b   blink"
           echo
           echo "    fc bc   $( sed -r "$sed" <<< ${clnam[*]}  )"
           echo "       fl   $( sed -r "s/[^ ]/$e[1;33m&$off/g" <<< ${!fglev[*]} )"
           echo "       bl   $( sed -r "$sed" <<< ${bgnam[*]}  )"
           echo
           echo "A variable called <fc><fl><bc><bl>[u][b] will be set with the corresponding ansi code."
           echo

    esac

  }



  gLhelp()
  {

    gLstatusupd

    cat << EOT

Uso: ${y1kd}$script${off} <${g1kd}ação${off}>

Ações:

$(
   cat $source \
   | sed "
           /case \"\$action\"/,/esac/!d
           /) \+#./!d
           s/^ */${g1kd}/
           s/|./${w1kd}/
           s/ \+) \+# \+/${off}:/
           s/\([^$e]\)\[/\1${w1kd}/
           s/\]/${off}/
           s/:</ </
           s/^/     /
         " \
   | column0 : \
   | sed "
           s/</<${b1kd}/
           s/>/${off}>/
         "
 )

Biblioteca:

$(
   cat $source \
   | grep -P "[g_].+\(\)" \
   | sed -r "
              /^_+LIB/,/^_+TODO/!d
              /^_+/d;s/\(\) +#/${off}=/
              s/^  g/     ${y1kd}g${w1kd}/
            " \
   | sort \
   | column0 =
 )

Cliente selecionado: ${b1kd}$c_name${off}
Token expira em ${w1kd}${gapi[$c_name-token_expires_at]}${off} (mais ${w1kd}${gapi[$c_name-token_expiring_in]}${off}s)

Credenciais de autenticação dos clientes: ${w1kd}~/.gapi${off}
Estado      de autorização  dos clientes: ${w1kd}~/.gapi-*${off}

Debug ${y1kd}$($debug || echo -n DES)ATIVADO${off}

EOT

  }



  gLstatusupd()
  {
    gapi[$c_name-user_code_expiring_in]=$(( gapi[$c_name-user_code_expires_ts] - $(date +%s) ))
    gapi[$c_name-token_expiring_in]=$(( gapi[$c_name-token_expires_ts] - $(date +%s) ))
  }



  gLstatus()
  {
    gLstatusupd
    debug=true gLdump $(echo ${!gapi[*]} | tr ' ' '\n' | grep $c_name | cut -d- -f2 | sort)
  }



  gLrevoke()
  {
    echo "[7] ${y1kd}revogar a autorização${off}"
    if [[ ${gapi[$c_name-refresh_token]} ]]
    then
      gurl="https://accounts.google.com/o/oauth2/revoke?token=${gapi[$c_name-access_token]}"
      gapi[$c_name-resp7]=$(curl $gurl \
                              -s \
                              -H Content-type:application/x-www-form-urlencoded)
      gLdump resp7
      keys=$(echo ${!gapi[*]} | tr ' ' '\n' | grep $c_name | sed 's/^/gapi[/;s/$/]/')
      unset $keys
      rm $gapist
    else
      echo "    Nenhuma autorização para revogar."
    fi
  }



  gLtoken()
  {
    echo "[4] ${y1kd}obter o token de acesso e o token de atualização do token de acesso${off}"
    data="client_id=${gapi[$c_name-client_id]}&client_secret=${gapi[$c_name-client_secret]}&grant_type=http://oauth.net/grant_type/device/1.0&code=${gapi[$c_name-device_code]}"
    gurl="https://www.googleapis.com/oauth2/v4/token"
    gapi[$c_name-resp4]=$(curl $gurl \
                            -s \
                            -d "$data") || { echo "    ${r1kd}Sem acesso à nuvem${off} $sorry_ico" > /dev/stderr; return ${gERR[nocloud]}; }
    gLdump   resp4
    gLjsonid resp4 error access_token refresh_token expires_in
    gapi[$c_name-error4]=${gapi[$c_name-error]}
    unset gapi[$c_name-error]
    if [[ ${gapi[$c_name-expires_in]} ]]
    then
      gapi[$c_name-token_expires_in]=${gapi[$c_name-expires_in]}
      gapi[$c_name-token_expires_ts]=$(LC_ALL=C date +'%s' -d "now + ${gapi[$c_name-token_expires_in]} sec")
      gapi[$c_name-token_expires_at]=$(LC_ALL=C date +'%Y/%m/%d %a %H:%M:%S %Z' -d @${gapi[$c_name-token_expires_ts]})
      unset gapi[$c_name-expires_in]
    fi
    gLdump error4 access_token refresh_token token_expires_in token_expires_ts token_expires_at
  }



  gLauth()
  {

    # GLOBALS: debug c_name

    if [[ ${gapi[$c_name-refresh_token]} ]]
    then

      echo "[6] ${y1kd}atualizar o access_token usando o refresh_token [${w1kd}${gapi[$c_name-refresh_token]}${y1kd}]${off}"
      data="client_id=${gapi[$c_name-client_id]}&client_secret=${gapi[$c_name-client_secret]}&grant_type=refresh_token&refresh_token=${gapi[$c_name-refresh_token]}"
      gurl="https://accounts.google.com/o/oauth2/token"
      gapi[$c_name-resp6]=$(curl $gurl \
                              -s \
                              -d "$data") || { echo "    ${r1kd}Sem acesso à nuvem${off} $sorry_ico" > /dev/stderr; return ${gERR[nocloud]}; }
      gLdump   resp6
      gLjsonid resp6 error access_token expires_in
      gapi[$c_name-error6]=${gapi[$c_name-error]}
      unset gapi[$c_name-error]
      if [[ ${gapi[$c_name-expires_in]} ]]
      then
        gapi[$c_name-token_expires_in]=${gapi[$c_name-expires_in]}
        gapi[$c_name-token_expires_ts]=$(LC_ALL=C date +'%s' -d "now + ${gapi[$c_name-token_expires_in]} sec")
        gapi[$c_name-token_expires_at]=$(LC_ALL=C date +'%Y/%m/%d %a %H:%M:%S %Z' -d @${gapi[$c_name-token_expires_ts]})
        unset gapi[$c_name-expires_in]
      fi
      gLdump error6 access_token token_expires_in token_expires_ts token_expires_at
      token_update=true

    else

      echo "[1] ${y1kd}definir uma credencial oauth (id de cliente) para o aplicativo cliente${off}"
      gLdump c_name client_id client_secret
      $debug && echo

      echo "[2] ${y1kd}definir o escopo de atuação${off}"
      gLdump scope
      $debug && echo

      echo "[3] ${y1kd}requisitar os códigos de autorização (device e user) para o escopo definido${off}"
      data="client_id=${gapi[$c_name-client_id]}&scope=${gapi[$c_name-scope]}"
      gurl="https://accounts.google.com/o/oauth2/device/code"
      gapi[$c_name-resp3]=$(curl $gurl \
                              -s \
                              -d "$data") || { echo "    ${r1kd}Sem acesso à nuvem${off} $sorry_ico" > /dev/stderr; return ${gERR[nocloud]}; }
      gLdump   resp3
      gLjsonid resp3 error device_code user_code expires_in verification_url
      gapi[$c_name-error3]=${gapi[$c_name-error]}
      unset gapi[$c_name-error]
      if [[ ${gapi[$c_name-expires_in]} ]]
      then
        gapi[$c_name-user_code_expires_in]=${gapi[$c_name-expires_in]}
        gapi[$c_name-user_code_expires_ts]=$(LC_ALL=C date +'%s' -d "now + ${gapi[$c_name-user_code_expires_in]} sec")
        gapi[$c_name-user_code_expires_at]=$(LC_ALL=C date +'%Y/%m/%d %a %H:%M:%S %Z' -d @${gapi[$c_name-user_code_expires_ts]})
        unset gapi[$c_name-expires_in]
      fi
      gLdump error3 device_code user_code user_code_expires_in user_code_expires_ts user_code_expires_at verification_url
      [[ ${gapi[$c_name-error3]} ]] && return ${gERR[other]}
      $debug && echo

      gLtoken "[4]"
      [[ ${gapi[$c_name-error4]} != authorization_pending ]] && return ${gERR[other]}
      while [[ ${gapi[$c_name-error4]} ]]
      do
        $debug && echo
        $debug && n= || n=-n
        echo    "[5] ${y1kd}autorizar a requisição${off}"
        echo    "    Pressione [${w1kd}Ctrl+C${off}] para cancelar ${y1kd}OU${off} registre o código [${w1kd}${gapi[$c_name-user_code]}${off}] no endereço [${w1kd}${gapi[$c_name-verification_url]}${off}]"
        echo    "    Este código é válido por ${w1kd}${gapi[$c_name-user_code_expires_in]}${off}s ou até você usá-lo (antes de ${w1kd}${gapi[curl-user_code_expires_at]}${off})"
        echo    "    Após o cadastro, pressione [${w1kd}ENTER${off}] para finalizar o processo de autorização."
        echo $n "    ${b1kd}Os aplicativos autorizados podem ser gerenciados${off} em [${w1kd}https://myaccount.google.com/permissions${off}]"
        unset gapi[$c_name-error4]
        read
        gLtoken "[4]"
      done
      token_update=false

    fi

    echo -e "    O token de acesso expira em $w1kd${gapi[$c_name-token_expires_at]}$off"
    $token_update || echo -e "    Para atualizar a autorização, apenas reexecute o comando:\n    ${g1kd}$script auth${off}"
    $debug && echo
    echo -e "[7] ${y1kd}para revogar a autorização:${off}\n    ${g1kd}$script revoke${off}"

    gLstatus | sed -r "s/$e\[[^m]+m//g" | cut -c5- >| $gapist

  }



# # <http://locutus.io/php/filesystem/realpath/>
#
# for d in .. ../ ../d . ./ ./d d d/ .d .d/ / /d
# {
#   [[ $d =~ ^\.\.(/|$) ]] && echo acima || { [[ $d =~ ^/ ]] && echo raiz || echo aqui; }
# }
#
# pwd # /home/paulo.bagatini
# readlink -m /../abc/./../123/oi/.. # /123
# readlink -m  ../abc/./../123/oi/.. # /home/123



  gLdrv() # common curl parameters
  {

    local resp error

    [[ $c_name ]] || { echo -e "Selecione um cliente com\n  ${y1kd}gapi client${off}" > /dev/stderr; return ${gERR[noclient]}; }

    resp=$(curl \
             -s \
             -H "Authorization: Bearer ${gapi[$c_name-access_token]}" \
             "$@") || { echo "    ${r1kd}Sem acesso à nuvem${off} $sorry_ico" > /dev/stderr; return ${gERR[nocloud]}; }

  # gapi[$c_name-resp8]=$resp
  # echo "${gapi[$c_name-resp8]}" > /dev/stderr
  # gLdump   resp8
  # gLjsonid resp8 error
  # gapi[$c_name-error8]=${gapi[$c_name-error]}

    if grep -wq error <<< $resp
    then
      typeset -A error
      . <(echo 'error=('; grep -wE 'code|reason|message' <<< $resp | sed -r 's/^ */[/;s/[,]//g;s/: /]=/'; echo ')')
      echo -e "Erro: ${error[message]} (${error[code]})" > /dev/stderr
      gLstatusupd
      echo "      Token expirado em ${w1kd}${gapi[$c_name-token_expires_at]}${off} (${w1kd}${gapi[$c_name-token_expiring_in]}${off}s)" > /dev/stderr
      return ${gERR[other]}
    else
      echo "$resp"
    fi

  }



  gLrealpath() # canonicalize path
  {
    echo "$(
             builtin cd /
             [[ ${1:0:1} == / ]] && r= || r="$gPWD/"
             readlink -m "$r$1"
           )"
  }



  gLdata()
  {
    prop=$(sed 's/#.*//; /^ *$/d' <<< $1)
    echo {${prop//$'\n'/,} }
  }



  gLdump()
  {
    $debug && for i in $*
    {
      [[ ${gapi[$c_name-$i]} ]] && echo "gapi[$b1kd$c_name$off-$r1kd$i$off]='$w1kd${gapi[$c_name-$i]}$off'"
    } | sed 's/^/    /'
  }



  gLjsonid()
  {
    . <(
         resp=$1; shift
         keys=$*
         echo -n  ${keys// /|}                            | sed -r "s/(\||$)/]=\n/g;s/^|(\n)(.)/\1gapi[$c_name-\2/g"
         grep -wE ${keys// /|} <<< ${gapi[$c_name-$resp]} | sed -r "s/[ \",]//g;s/:/]=/;s/^/gapi[$c_name-/g"
       )
  }



  gLdebug()
  {
    [[ $1 == -n ]] && { local n=$1; shift; }
    ${debug:-false} && echo $n "$*" > /dev/stderr
  }



  gLmkd1l() # make directory given his parent
  {

    local parent_id name desc type parent prop gurl resp id stat data

    parent_id=$1
         name=$2
         desc=$3
         type=folder
       parent=$(printf %-$((33+13))s "'$w1kd$parent_id$off'")

    gLdebug -n "parent=$parent ; "

    gurl="https://www.googleapis.com/drive/v3/files?fields=files(id)&q=trashed=false+and+mimeType='application/vnd.google-apps.$type'+and+'$parent_id'+in+parents+and+name='${name// /+}'"
    resp=$(gLdrv $gurl) || return $?
    unset id
    . <(grep id <<< $resp | sed -r 's/[",]//g;s/: /=/')

    if [[ $id ]]
    then
      stat=exists
    else
      stat=create

      prop="
             title: '$name'
             mimeType: 'application/vnd.google-apps.$type'
             parents: [ { id:'$parent_id' } ]
           "

      [[ $desc ]] && \
      prop="
             $prop
             description: '$desc'
           "

    # echo "prop=$prop" > /dev/stderr

      data=$(gLdata "$prop")
      gurl="https://www.googleapis.com/drive/v2/files?fields=id"
      resp=$(gLdrv $gurl \
               -X POST \
               -H Content-Type:application/json \
               -d "$data")
      unset id
      . <(grep id <<< $resp | sed -r 's/[",]//g;s/: /=/')

    fi

    gLdebug "id='$w1kd$id$off' ; stat='$w1kd$stat$off' ; dir='$w1kd$name$off' ; desc='$w1kd$desc$off'"

    echo $id

  }



  gLwdcd() # change working directory
  {

    local path gdir parent_id dir id gurl desc

    path=$([[ $1 == - ]] && echo $gOWD || gLrealpath "$1")

    gdir=${path:1}
    type=folder

    parent_id=root
    [[ $gdir ]] && while read dir
    do

      gurl="https://www.googleapis.com/drive/v3/files?fields=files(id)&q=trashed=false+and+mimeType='application/vnd.google-apps.$type'+and+'$parent_id'+in+parents+and+name='${dir// /+}'"
      resp=$(gLdrv $gurl) || { echo -n "$resp"; return $?; }
      unset id
      . <(grep id <<< $resp | sed -r 's/[",]//g;s/: /=/')

      if [[ $id ]]
      then
        stat='exists'
        gLdebug -n "id='$w1kd$id$off' ; "
      else
        stat='!found'
        gLdebug -n "                                         "
      fi

      gLdebug "stat='$w1kd$stat$off' ; dir='$w1kd$dir$off'"

      [[ $stat == exists ]] || { echo "Caminho [${w1kd}$path${off}] não encontrado"; return ${gERR[notfound]}; }

      parent_id=$id

    done <<< "${gdir////$'\n'}"

    gOWD=$gPWD
    gPWD=$path

  }



  gLlwdcd() # change last working directory per tty
  {

    local WD

    [[ $1 != : ]] \
    && WD=$1 \
    || {
         [[ -s "$gLWDFILE" ]] && WD=$(grep -w "$gTTY" "$gLWDFILE" | cut -f3)
       }

    gLwdcd "${WD:-/}" \
    && {
         [[ -s "$gLWDFILE" ]] \
         && {
              grep -wv "$gTTY" "$gLWDFILE" >| "$gLWDFILE-$$"
              $(which -a mv | grep / | tail -n1) "$gLWDFILE-$$" "$gLWDFILE" # para contornar alias de segurança para mv
            }
         [[ $gPWD != / ]] && echo -e "$gTTY\t$(/bin/date +'%Y/%m/%d %a %H:%M:%S %z')\t$gPWD" >> "$gLWDFILE"
       }

  }



___COMPLETE()(:) # >>------------------------------------------------------------------------------<< #



  gLcomp()
  {
    # <http://iridakos.com/tutorials/2018/03/01/bash-programmable-completion-tutorial.html>
    complete -F gLcomp_gapi gapi
    complete -F gLcomp_fs   gls gmkdir gshare gtrash grm gmkfile gcat gupdate gmv gcp
  }



  gLcomp_gapi()
  {
    (( ${#COMP_WORDS[@]} > 2 )) && return
    local words=$(
      sed "
            /case \"\$action\"/,/esac/!d
            /) \+#./!d
            s/\(^ *\||.\| \+) \+#.*\|\]\)//g
          " $source
    )
    COMPREPLY=( $( compgen -W "$words" "${COMP_WORDS[1]}" ) )
  }

  gLcomp_cd()
  {
    (( ${#COMP_WORDS[@]} > 2 )) && return
    local words=$(
      gls | grep name | cut -d\" -f4
    )
    COMPREPLY=( $( compgen -W "$words" "${COMP_WORDS[1]}" ) )
  }

  gLcomp_fs()
  {
    (( ${#COMP_WORDS[@]} > 2 )) && return
    local words=$(
      gls | grep name | cut -d\" -f4
    )
    COMPREPLY=( $( compgen -W "$words" "${COMP_WORDS[1]}" ) )
  }



________LIB()(:) # >>------------------------------------------------------------------------------<< #



  gapi() # gerencia os tokens e a biblioteca
  {

    source=$(readlink -f $BASH_SOURCE)
    script=$FUNCNAME
    action=$1

    gapicf=$HOME/.gapi
    gapist=$HOME/.gapi-$c_name

    if [[ ! -e $gapicf ]]
    then
      echo "Arquivo de configuração $b1kd$gapicf$off não encontrado. Foi criado um modelo que precisa ser personalizado para uso da biblioteca."
      cat << EOT >| $gapicf
# vim: ft=sh

# $gapicf

  [ curl-client_id     ] = 000000000000-00000000000000000000000000000000.apps.googleusercontent.com
  [ curl-client_secret ] = 000000-00000000000000000
  [ curl-scope         ] = https://www.googleapis.com/auth/drive.file+https://www.googleapis.com/auth/docs
                         # <https://developers.google.com/identity/protocols/googlescopes>
                         # ver e gerenciar os arquivos e pastas do Google Drive que você abriu ou criou com este aplicativo
                         # ver, editar, criar e excluir seus documentos do Google Drive

# EOF
EOT
      return 1
    fi

#   if [[ $c_name || $action != c && $action != client ]]
#   then
      typeset -gA gapi
      if [[ ! ${gapi[$c_name-refresh_token]} ]]
      then
        if [[ -e $gapist ]]
        then
          . $gapist
        else
          eval $(sed -rn "s/(#.*|[ \t]+)//g; /^$/d; s/^/gapi/; /$c_name/p" $gapicf)
        fi
      fi
#   fi

    keys=${!gapi[*]}

    debug=${debug:-false}

    case "$action" in

      l|list    ) # lista os clientes configurados em [~/.gapi]
                  echo -e "Clientes cadastrados em [${w1kd}$gapicf${off}]"
                  echo -e "${keys// /\\n}" | cut -d- -f1 | sort -u | sed "s/^/  ${b1kd}/;s/$/${off}/"
                  ;;

      c|client  ) # <cliente>:seleciona o cliente a ser usado
                  [[ $2      ]] || { gLhelp; return ${gERR[help]}; }
                  echo -e "${keys// /\\n}" | cut -d- -f1 | grep -wq $2 || { echo "Cliente $b1kd$2$off não configurado"; return ${gERR[other]}; }
                  c_name=$2
                  echo "Cliente $b1kd$c_name$off selecionado"
                  ;;

      a|auth    ) # inicializa ou atualiza a autorização do cliente selecionado
                  [[ $c_name ]] || { echo -e "Selecione um cliente com\n  ${y1kd}gapi client${off}\n" > /dev/stderr; return ${gERR[noclient]}; }
                  gLauth
                  ;;

      r|revoke  ) # revoga a autorização do cliente selecionado
                  [[ $c_name ]] || { echo -e "Selecione um cliente com\n  ${y1kd}gapi client${off}\n" > /dev/stderr; return ${gERR[noclient]}; }
                  gLrevoke
                  ;;

      s|status  ) # exibe os dados do cliente selecionado
                  [[ $c_name ]] || { echo -e "Selecione um cliente com\n  ${y1kd}gapi client${off}\n" > /dev/stderr; return ${gERR[noclient]}; }
                  echo -e "Dados do cliente:\n"
                  gLstatus
                  echo -en "\nDebug ${y1kd}"; $debug || echo -n DES; echo -e "ATIVADO${off}"
                  ;;

      f|forget  ) # remove a biblioteca
                  type -t sp | grep -q function && sp || gsp ori
                  unset -f $(grep -oP '[g_].+(?=\(\))' $source)
                  unset gapi c_name
                  echo "Bibliteca $lib_name removida"
                  ;;

      v|verbose ) # ativa o modo debug
                  debug=true
                  echo "Debug ${y1kd}ATIVADO${off}"
                  ;;

      q|quiet   ) # desativa o modo debug
                  debug=false
                  echo "Debug ${y1kd}DESATIVADO${off}"
                  ;;

      u|update  ) # recarrega as definições das funções de acesso à API
                  . $source
                  ;;

      *         ) #
                  gLhelp
                  return ${gERR[help]}
                  ;;

    esac

  }

  export -f gapi



  gsp() # set prompt
  {

    [[ $1 == -h ]] && \
    {
      echo "
Configura o prompt para visualização dos diretórios local e remoto.

Uso: $FUNCNAME [opção]

Opções:

     -h        esta mensagem de ajuda
     def       prompt costumeiramente default
     ori       prompt original antes de ser alterado
     <string>  inclui string como prefixo do prompt
"
      return
    }

    local TTY=$( ( tty || echo unk ) | cut -d/ -f3- )

    local   e=$'\033'
    local bla="\[$e[1;30m\]"
    local red="\[$e[1;31m\]"
    local gre="\[$e[1;32m\]"
    local yel="\[$e[1;33m\]"
    local blu="\[$e[1;34m\]"
    local mag="\[$e[1;35m\]"
    local cya="\[$e[1;36m\]"
    local whi="\[$e[1;37m\]"
    local off="\[$e[0m\]"

    local prefc=$yel
    local  defc=$red
    local numbc=$red
    local userc=$gre
    local hostc=$yel
    local  ttyc=$cya
    local pathc=$whi
    local gpwdc=$whi
  # local hostn='\h'
    local hostn='$(hostname|cut -d. -f1)'

    local chr at ab fe
    if (( $(id -u) == 0 ))
    then
      chr="#"
       at=""
    else
      chr="$"
       at="$userc\u$defc@"
    fi
    gchr=$chr
    ab='['
    fe=']'

    local  title="\[$e]0;$ab$TTY$fe$hostn:\w\a\]"
    local prompt="$numbc\! $ab$ttyc$TTY$defc$fe$at$hostc$hostn$defc:$pathc\w$defc$chr$gpwdc\$gPWD$defc$gchr$off "

    dPS1="[\u@\h \W]\\$ "
    oPS1=${oPS1:-$PS1}

    case $* in
    def ) PS1="$dPS1";;
    ori ) PS1="$oPS1";;
    *   ) local pref=$([[ $* ]] && echo "$prefc$*$off" || echo "$lib_name")" "
          PS1="$pref$title$prompt"
    esac

    export PS1

  }



  gplwd() # print last working directory per tty
  {
    [[ -s "$gLWDFILE" ]] && sort -t/ -k2 -n "$gLWDFILE" | sed -r "s|^|  |; s|^ ( *$gTTY\t)|*\1|" | column0
  }



  gpwd() # print working directory
  {
    echo "$gPWD"
  }



  gcd() # change directory
  {
    gLlwdcd $1
  }



  gls() # lista o conteúdo do diretório
  {

    local type path gdir name parent_id dir id gurl resp q sed
    type=folder

    path=$(gLrealpath "$1")
    gdir=${path:1}

    parent_id=root
    [[ $gdir ]] && \
    while read dir
    do
      gurl="https://www.googleapis.com/drive/v3/files?fields=files(id)&q=trashed=false+and+mimeType='application/vnd.google-apps.$type'+and+'$parent_id'+in+parents+and+name='${dir// /+}'"
      resp=$(gLdrv $gurl) || return $?
      unset id
      . <(grep id <<< $resp | sed -r 's/[",]//g;s/: /=/')
      parent_id=$id
    done <<< "${gdir////$'\n'}"

    [[ $id ]] || { echo 'not found'; return ${gERR[notfound]}; }

     fields="isAppAuthorized,mimeType,shared,permissionIds,size,modifiedTime,id,name,description,trashed"
  #  fields="*"
       gurl="https://www.googleapis.com/drive/v3/files?fields=files($fields)&q='$parent_id'+in+parents$q"
       resp=$(gLdrv $gurl) || return $?



   # test=" text \n"
     test=

     ac b1kd  "$test" # azul   1 em fundo preto                 - diretório            autorizado privado
     ac k4bl  "$test" # preto  4 em fundo azul  claro           - diretório            autorizado privado       na lixeira
     ac c3kd  "$test" # ciano  3 em fundo preto                 - diretório            autorizado compartilhado
     ac k4cd  "$test" # preto  4 em fundo ciano escuro          - diretório            autorizado compartilhado na lixeira

     ac g3kd  "$test" # verde  3 em fundo preto                 - arquivo editável     autorizado privado
     ac k4gd  "$test" # preto  4 em fundo verde escuro          - arquivo editável     autorizado privado       na lixeira
     ac g1kd  "$test" # verde  1 em fundo preto                 - arquivo editável     autorizado compartilhado
     ac k4gl  "$test" # preto  4 em fundo verde claro           - arquivo editável     autorizado compartilhado na lixeira

     ac w3kd  "$test" # branco 3 em fundo preto                 - arquivo genérico     autorizado privado
     ac k4wd  "$test" # preto  4 em fundo branco                - arquivo genérico     autorizado privado       na lixeira
     ac w1kd  "$test" # branco 1 em fundo preto                 - arquivo genérico     autorizado compartilhado
     ac k4wl  "$test" # preto  4 em fundo branco                - arquivo genérico     autorizado compartilhado na lixeira

     ac b1kdb "$test" # azul   1 em fundo preto        piscando - diretório        não autorizado privado
     ac k4blb "$test" # preto  4 em fundo azul  claro  piscando - diretório        não autorizado privado       na lixeira
     ac c3kdb "$test" # ciano  3 em fundo preto        piscando - diretório        não autorizado compartilhado
     ac k4cdb "$test" # preto  4 em fundo ciano escuro piscando - diretório        não autorizado compartilhado na lixeira

     ac g3kdb "$test" # verde  3 em fundo preto        piscando - arquivo editável não autorizado privado
     ac k4gdb "$test" # preto  4 em fundo verde escuro piscando - arquivo editável não autorizado privado       na lixeira
     ac g1kdb "$test" # verde  1 em fundo preto        piscando - arquivo editável não autorizado compartilhado
     ac k4glb "$test" # preto  4 em fundo verde claro  piscando - arquivo editável não autorizado compartilhado na lixeira

     ac w3kdb "$test" # branco 3 em fundo preto        piscando - arquivo genérico não autorizado privado
     ac k4wdb "$test" # preto  4 em fundo branco       piscando - arquivo genérico não autorizado privado       na lixeira
     ac w1kdb "$test" # branco 1 em fundo preto        piscando - arquivo genérico não autorizado compartilhado
     ac k4wlb "$test" # preto  4 em fundo branco       piscando - arquivo genérico não autorizado compartilhado na lixeira



     npadirc=$b1kd  #         private appauth dir
     tpadirc=$k4bl  # trashed private appauth dir
     nsadirc=$c3kd  #         shared  appauth dir
     tsadirc=$k4cd  # trashed shared  appauth dir

     npafilc=$g3kd  #         private appauth file
     tpafilc=$k4gd  # trashed private appauth file
     nsafilc=$g1kd  #         shared  appauth file
     tsafilc=$k4gl  # trashed shared  appauth file

     npagenc=$w3kd  #         private appauth generic file
     tpagenc=$k4wd  # trashed private appauth generic file
     nsagenc=$w1kd  #         shared  appauth generic file
     tsagenc=$k4wl  # trashed shared  appauth generic file

     npndirc=$b1kdb #         private notauth dir
     tpndirc=$k4blb # trashed private notauth dir
     nsndirc=$c3kdb #         shared  notauth dir
     tsndirc=$k4cdb # trashed shared  notauth dir

     npnfilc=$g3kdb #         private notauth file
     tpnfilc=$k4gdb # trashed private notauth file
     nsnfilc=$g1kdb #         shared  notauth file
     tsnfilc=$k4glb # trashed shared  notauth file

     npngenc=$w3kdb #         private notauth generic file
     tpngenc=$k4wdb # trashed private notauth generic file
     nsngenc=$w1kdb #         shared  notauth generic file
     tsngenc=$k4wlb # trashed shared  notauth generic file



    prelist=$(
      {
        echo $( gstat .  | sed -r 's/("name": ")[^"]+/\1./'  )
        echo $( gstat .. | sed -r 's/("name": ")[^"]+/\1../' )
        echo $resp
      } | grep -oP '{[^{}]+}' \
        | sort -t: -k3 \
        | sed -r "

          /vnd.google-apps.folder/   { /\"shared\": false/ { /\"trashed\": false/ s/(\"name\": \")([^\"]+)(\")/\1$npadirc\2$off\3/
                                                             /\"trashed\": true/  s/(\"name\": \")([^\"]+)(\")/\1$tpadirc\2$off\3/ }
                                       /\"shared\": true/  { /\"trashed\": false/ s/(\"name\": \")([^\"]+)(\")/\1$nsadirc\2$off\3/
                                                             /\"trashed\": true/  s/(\"name\": \")([^\"]+)(\")/\1$tsadirc\2$off\3/ } }

          /vnd.google-apps.document/ { /\"shared\": false/ { /\"trashed\": false/ s/(\"name\": \")([^\"]+)(\")/\1$npafilc\2$off\3/
                                                             /\"trashed\": true/  s/(\"name\": \")([^\"]+)(\")/\1$tpafilc\2$off\3/ }
                                       /\"shared\": true/  { /\"trashed\": false/ s/(\"name\": \")([^\"]+)(\")/\1$nsafilc\2$off\3/
                                                             /\"trashed\": true/  s/(\"name\": \")([^\"]+)(\")/\1$tsafilc\2$off\3/ } }

          /vnd.google-apps.(folder|document)/! { /\"shared\": false/ { /\"trashed\": false/ s/(\"name\": \")([^\"]+)(\")/\1$npagenc\2$off\3/
                                                                       /\"trashed\": true/  s/(\"name\": \")([^\"]+)(\")/\1$tpagenc\2$off\3/ }
                                                 /\"shared\": true/  { /\"trashed\": false/ s/(\"name\": \")([^\"]+)(\")/\1$nsagenc\2$off\3/
                                                                       /\"trashed\": true/  s/(\"name\": \")([^\"]+)(\")/\1$tsagenc\2$off\3/ } }

        "
    )


    sed='
          /"size":/!s/, "isAppAuthorized":/, "size": "0"&/
          s/"?,? "[^"]+": "?/\t/g
          s/(^\{\t| \}$|\[ | \]|application\/vnd.google-apps\.)//g
          s|[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\.[0-9]{3}Z|$(LC_ALL=C date -d & +"%Y/%m/%d %a %H:%M:%S.%3N %Z")|g
        '

    dotpat=m\.\.?$'\033'


  # echo "$resp"
  # echo
  # {
  #   echo $( gstat .  | sed -r 's/("name": ")[^"]+/\1./'  )
  #   echo $( gstat .. | sed -r 's/("name": ")[^"]+/\1../' )
  #   echo $resp
  # } | grep -oP '{[^{}]+}' | sort -t: -k3
  # echo
  # echo "$prelist"
  # echo
  # echo "$prelist" | sed -r "$sed" | column0
  # echo


    {
      echo -e "id\tname\tmimeType\ttrashed\tmodifiedTime\tshared\tpermissionIds\tsize\tappAuth"
      eval "echo \"$(
      {
        grep -E   $dotpat <<< $prelist | tac
        grep -Ew   folder <<< $prelist | grep -Ev $dotpat
        grep -Ewv "folder|$dotpat" <<< $prelist
      } | sed -r "$sed")\""
    } | awk -F'\t' '{ f="'\''6"(NR==1?"s":"d")
                      l=""
                      split($7,arr,", "); asort(arr)
                      for (i=1; i<length(arr); i++) l=l arr[i] ","; l=l arr[i]
                      printf "%s\t%"f"\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n", \
                              $3, $8,   $5, $2, $6, $4, $9, $1, l  }' \
      | sed "1{s/^/$w1kd/;s/$/$off/}" \
      | column0

  }



  gmkdir() # cria hierarquia de diretórios
  {

    local path gdir name desc parent_id dir id

    path=$(gLrealpath "$1")
    name=${path##*/}

    gdir=${path%/*}
    gdir=${gdir:1}

    desc=$2

  # echo "path=$path"
  # echo "gdir=$gdir"
  # echo "name=$name"

    parent_id=root
    [[ $gdir ]] && while read dir
    do
      id=$(gLmkd1l $parent_id "$dir" "") || return $?
    # echo " dir=$dir"
    # echo "  id=$id"
      parent_id=$id
    done <<< "${gdir////$'\n'}"

    id=$(gLmkd1l $parent_id "$name" "$desc")

  # echo " dir=$name"
  # echo "  id=$id"

  }



  gshare() # compartilha arquivo/diretório
  {

    local path gdir type parent_id dir name gurl resp fields perm_id element mimeType prop data

    if [[ ! $2 =~ ^(false|true|sotrue)$ ]]
    then
      echo "Use: $FUNCNAME <elemento> false|true|sotrue"
      return ${gERR[other]}
    fi

    path=$(gLrealpath "$1")

    gdir=${path%/*}
    gdir=${gdir:1}

    type=folder

    parent_id=root
    [[ $gdir ]] && while read dir
    do
      echo " dir=$dir"
      name=$dir
      gurl="https://www.googleapis.com/drive/v3/files?fields=files(id)&q=trashed=false+and+mimeType='application/vnd.google-apps.$type'+and+'$parent_id'+in+parents+and+name='${name// /+}'"
      resp=$(gLdrv $gurl) || return $?
      unset id
      . <(grep id <<< $resp | sed -r 's/[",]//g;s/: /=/')
      parent_id=$id
    done <<< "${gdir////$'\n'}"

    name=${path##*/}

    fields="mimeType,id,permissionIds"
    gurl="https://www.googleapis.com/drive/v3/files?fields=files($fields)&q=trashed=false+and+'$parent_id'+in+parents+and+name='${name// /+}'"
    resp=$(gLdrv $gurl) || return $?
    unset mimeType id
    . <(grep -E 'mimeType|id' <<< $resp | sed -r 's/[",]//g;s/: /=/')
    perm_id=$(grep -oE 'anyone(|WithLink)' <<< $resp)

    element=$([[ $mimeType =~ .+folder ]] && echo " dir" || echo "file")

    echo "$element=$name"

    [[ $id ]] || { echo 'not found'; return ${gERR[notfound]}; }

    gurl="https://www.googleapis.com/drive/v3/files/$id/permissions"

    case $2 in

    false  ) # owner
             resp=$(gLdrv $gurl/$perm_id -X DELETE)
             ;;

    true   ) # anyone
             prop="
                    role: 'reader'
                    type: 'anyone'
                    allowFileDiscovery: false
                  "
             data=$(gLdata "$prop")
             resp=$(gLdrv $gurl \
                      -X POST \
                      -H Content-Type:application/json \
                      -d "$data")
             ;;

    sotrue ) # anyoneWithLink
             prop="
                    role: 'reader'
                    type: 'anyone'
                    allowFileDiscovery: true
                  "
             data=$(gLdata "$prop")
             resp=$(gLdrv $gurl \
                      -X POST \
                      -H Content-Type:application/json \
                      -d "$data")
             ;;

    esac

    echo "$resp"

  }



  gtrash() # envia/recupera arquivo/diretório para/da lixeira
  {

    local path gdir type parent_id dir name gurl resp fields element mimeType prop data

    if [[ ! $2 =~ ^(false|true)$ ]]
    then
      echo "Use: $FUNCNAME <elemento> false|true"
      return ${gERR[other]}
    fi

    path=$(gLrealpath "$1")

    gdir=${path%/*}
    gdir=${gdir:1}

    type=folder

    parent_id=root
    [[ $gdir ]] && while read dir
    do
      echo " dir=$dir"
      name=$dir
      gurl="https://www.googleapis.com/drive/v3/files?fields=files(id)&q=trashed=false+and+mimeType='application/vnd.google-apps.$type'+and+'$parent_id'+in+parents+and+name='${name// /+}'"
      resp=$(gLdrv $gurl) || return $?
      unset id
      . <(grep id <<< $resp | sed -r 's/[",]//g;s/: /=/')
      parent_id=$id
    done <<< "${gdir////$'\n'}"

    name=${path##*/}

    fields="mimeType,id"
    gurl="https://www.googleapis.com/drive/v3/files?fields=files($fields)&q=trashed=$($2 && echo false || echo true)+and+'$parent_id'+in+parents+and+name='${name// /+}'"
    resp=$(gLdrv $gurl) #|| return $?
    unset mimeType id
    . <(grep -E 'mimeType|id' <<< $resp | sed -r 's/[",]//g;s/: /=/')

    element=$([[ $mimeType =~ .+folder ]] && echo " dir" || echo "file")

    echo "$element=$name"

    [[ $id ]] || { echo 'not found'; return ${gERR[notfound]}; }

  # gurl="https://www.googleapis.com/drive/v3/files/$id"

  # prop="trashed:$2"
  # data=$(gLdata "$prop")
  # resp=$(gLdrv $gurl \
  #          -X PATCH \
  #          -H Content-Type:application/json \
  #          -d "$data")
  # echo "$resp"

  }



  grm() # deleta definitivamente arquivo/diretório
  {

    local path gdir type parent_id dir name gurl resp fields element mimeType

    path=$(gLrealpath "$1")

    gdir=${path%/*}
    gdir=${gdir:1}

    type=folder

    parent_id=root
    [[ $gdir ]] && while read dir
    do
      echo " dir=$dir"
      name=$dir
      gurl="https://www.googleapis.com/drive/v3/files?fields=files(id)&q=trashed=false+and+mimeType='application/vnd.google-apps.$type'+and+'$parent_id'+in+parents+and+name='${name// /+}'"
      resp=$(gLdrv $gurl) || return $?
      unset id
      . <(grep id <<< $resp | sed -r 's/[",]//g;s/: /=/')
      parent_id=$id
    done <<< "${gdir////$'\n'}"

    name=${path##*/}

    fields="mimeType,id"
    gurl="https://www.googleapis.com/drive/v3/files?fields=files($fields)&q='$parent_id'+in+parents+and+name='${name// /+}'"
    resp=$(gLdrv $gurl) || return $?
    unset mimeType id
    . <(grep -E 'mimeType|id' <<< $resp | sed -r 's/[",]//g;s/: /=/')

    element=$([[ $mimeType =~ .+folder ]] && echo " dir" || echo "file")

    echo "$element=$name"

    [[ $id ]] || { echo 'not found'; return ${gERR[notfound]}; }

    gurl="https://www.googleapis.com/drive/v3/files/$id"
    resp=$(gLdrv $gurl -X DELETE)
    echo "$resp"

  }



  gmkfile() # envia texto plano para arquivo não existente
  {

    local path gdir desc body type parent_id dir name gurl resp fields element mimeType prop boundary data size

    path=$(gLrealpath "$1")

    gdir=${path%/*}
    gdir=${gdir:1}
    desc=$2
    [[ -t 0 ]] && { echo "Use: <conteúdo> | $FUNCNAME <arquivo> <descrição>"; return ${gERR[other]}; } || body=$(< /dev/stdin)

    type=folder

    parent_id=root
    [[ $gdir ]] && while read dir
    do
      echo " dir=$dir"
      name=$dir
      gurl="https://www.googleapis.com/drive/v3/files?fields=files(id)&q=trashed=false+and+mimeType='application/vnd.google-apps.$type'+and+'$parent_id'+in+parents+and+name='${name// /+}'"
      resp=$(gLdrv $gurl) || return $?
      unset id
      . <(grep id <<< $resp | sed -r 's/[",]//g;s/: /=/')
      echo "   id=$id"
      parent_id=$id
    done <<< "${gdir////$'\n'}"

    name=${path##*/}
    mimeType=document

    prop="
           name: '$name'
           mimeType: 'application/vnd.google-apps.$mimeType'
           parents: [ '$parent_id' ]
         "

    [[ $desc ]] && \
    prop="
           $prop
           description: '$desc'
         "

    boundary=foo_bar_baz

    data=$(gLdata "$prop")
    data=$(echo -en "\n--$boundary\nContent-Type: application/json\n\n$data\n\n--$boundary\nContent-Type: text/plain\n\n$body\n--$boundary--")
    echo "$data"
    size=$(LC_ALL=C; echo ${#data})
    echo "$size"

    gurl="https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart"
    resp=$(gLdrv $gurl \
             -X POST \
             -H "Content-Type: multipart/related; boundary=$boundary" \
             -H "Content-Length: $size" \
             -d "$data")
    echo "$resp"

  }



  gcat() # retorna como texto plano o conteúdo do arquivo
  {

    local path gdir type parent_id dir name gurl resp fields element

    path=$(gLrealpath "$1")

    gdir=${path%/*}
    gdir=${gdir:1}

    type=folder

    parent_id=root
    [[ $gdir ]] && while read dir
    do
      echo " dir=$dir"
      name=$dir
      gurl="https://www.googleapis.com/drive/v3/files?fields=files(id)&q=trashed=false+and+mimeType='application/vnd.google-apps.$type'+and+'$parent_id'+in+parents+and+name='${name// /+}'"
      resp=$(gLdrv $gurl) || return $?
      unset id
      . <(grep id <<< $resp | sed -r 's/[",]//g;s/: /=/')
      parent_id=$id
    done <<< "${gdir////$'\n'}"

    name=${path##*/}

    fields="id"
    gurl="https://www.googleapis.com/drive/v3/files?fields=files($fields)&q='$parent_id'+in+parents+and+name='${name// /+}'"
    resp=$(gLdrv $gurl) || return $?
    unset id
    . <(grep -E 'id' <<< $resp | sed -r 's/[",]//g;s/: /=/')

    [[ $id ]] || { echo 'not found'; return ${gERR[notfound]}; }

    gurl="https://www.googleapis.com/drive/v3/files/$id/export?mimeType=text/plain"
    resp=$(gLdrv $gurl)
    echo "$resp"

  }



  gupdate() # envia texto plano atualizando o conteúdo do arquivo
  {

    local path gdir type parent_id dir name gurl resp fields element mimeType body

    path=$(gLrealpath "$1")

    gdir=${path%/*}
    gdir=${gdir:1}
    [[ -t 0 ]] && { echo "Use: <conteúdo> | $FUNCNAME <arquivo>"; return ${gERR[other]}; } || body=$(< /dev/stdin)

    type=folder

    parent_id=root
    [[ $gdir ]] && while read dir
    do
      echo " dir=$dir"
      name=$dir
      gurl="https://www.googleapis.com/drive/v3/files?fields=files(id)&q=trashed=false+and+mimeType='application/vnd.google-apps.$type'+and+'$parent_id'+in+parents+and+name='${name// /+}'"
      resp=$(gLdrv $gurl) || return $?
      unset id
      . <(grep id <<< $resp | sed -r 's/[",]//g;s/: /=/')
      parent_id=$id
    done <<< "${gdir////$'\n'}"

    name=${path##*/}
    mimeType=document

    fields="id"
    gurl="https://www.googleapis.com/drive/v3/files?fields=files($fields)&q='$parent_id'+in+parents+and+name='${name// /+}'"
    resp=$(gLdrv $gurl) || return $?
    unset id
    . <(grep -E 'id' <<< $resp | sed -r 's/[",]//g;s/: /=/')

    [[ $id ]] || { echo 'not found'; return ${gERR[notfound]}; }

    echo "$body"
    gurl="https://www.googleapis.com/upload/drive/v3/files/$id?uploadType=media"
    resp=$(gLdrv $gurl -X PATCH -d "$body")
    echo "$resp"

  }



  gmv() # move/renomeia arquivo/diretório
  {

    local path1 gdir1 type1 parent_id1 dir1 name1 element1 mimeType1
    local path2 gdir2 type2 parent_id2 dir2 name2 element2 mimeType2
    local gurl resp fields prop data



    path1=$(gLrealpath "$1")
    gdir1=${path1%/*}
    gdir1=${gdir1:1}
    type1=folder
    parent_id1=root
    [[ $gdir1 ]] && while read dir1
    do
      echo "  dir1=$dir1"
      name1=$dir1
       gurl="https://www.googleapis.com/drive/v3/files?fields=files(id)&q=trashed=false+and+mimeType='application/vnd.google-apps.$type1'+and+'$parent_id1'+in+parents+and+name='${name1// /+}'"
       resp=$(gLdrv $gurl) || return $?
      unset id
      . <(grep id <<< $resp | sed -r 's/[",]//g;s/: /=/')
      id1=$id
      echo "   id1=$id1"
      parent_id1=$id1
    done <<< "${gdir1////$'\n'}"
        name1=${path1##*/}
       fields="mimeType,id"
         gurl="https://www.googleapis.com/drive/v3/files?fields=files($fields)&q=trashed=false+and+'$parent_id1'+in+parents+and+name='${name1// /+}'"
         resp=$(gLdrv $gurl) || return $?
    unset mimeType id
    . <(grep -E 'mimeType|id' <<< $resp | sed -r 's/[",]//g;s/: /=/')
          id1=$id
    mimeType1=$mimeType
     element1=$([[ $mimeType1 =~ .+folder ]] && echo "  dir1" || echo " file1")
    echo "$element1=$name1"
    echo "   id1=$id1"



    echo



    path2=$(gLrealpath "$2")
    gdir2=${path2%/*}
    gdir2=${gdir2:1}
    type2=folder
    parent_id2=root
    [[ $gdir2 ]] && while read dir2
    do
      echo "  dir2=$dir2"
      name2=$dir2
       gurl="https://www.googleapis.com/drive/v3/files?fields=files(id)&q=trashed=false+and+mimeType='application/vnd.google-apps.$type2'+and+'$parent_id2'+in+parents+and+name='${name2// /+}'"
       resp=$(gLdrv $gurl) || return $?
      unset id
      . <(grep id <<< $resp | sed -r 's/[",]//g;s/: /=/')
      id2=$id
      echo "   id2=$id2"
      parent_id2=$id2
    done <<< "${gdir2////$'\n'}"
        name2=${path2##*/}
       fields="mimeType,id"
         gurl="https://www.googleapis.com/drive/v3/files?fields=files($fields)&q=trashed=false+and+'$parent_id2'+in+parents+and+name='${name2// /+}'"
         resp=$(gLdrv $gurl) || return $?
    unset mimeType id
    . <(grep -E 'mimeType|id' <<< $resp | sed -r 's/[",]//g;s/: /=/')
          id2=$id
    mimeType2=$mimeType
     element2=$([[ $mimeType2 =~ .+folder ]] && echo "  dir2" || echo " file2")
    echo "$element2=$name2"
    echo "   id2=$id2"

    [[ $id1 ]] || { echo 'not found'; return ${gERR[notfound]}; }

    unset name query
    [[ $element2   =~ file2       ]] &&  name="name: '$name2'"
    [[ $id2                       ]] && query="?removeParents=$parent_id1&addParents=$id2"
    [[ $parent_id2 != $parent_id1 ]] && query="?removeParents=$parent_id1&addParents=$parent_id2"

    prop="$name"
    data=$(gLdata "$prop")

    gurl="https://www.googleapis.com/drive/v3/files/$id1$query"
    echo -e "$data\n$gurl"
    resp=$(gLdrv $gurl \
             -X PATCH \
             -H Content-Type:application/json \
             -d "$data")
    echo "$resp"

  }



  gcp() # copia arquivo/diretório para outro nome/diretório
  {

    local path1 gdir1 type1 parent_id1 dir1 name1 element1 mimeType1
    local path2 gdir2 type2 parent_id2 dir2 name2 element2 mimeType2
    local gurl resp fields prop data



    path1=$(gLrealpath "$1")
    gdir1=${path1%/*}
    gdir1=${gdir1:1}
    type1=folder
    parent_id1=root
    [[ $gdir1 ]] && while read dir1
    do
      echo "  dir1=$dir1"
      name1=$dir1
       gurl="https://www.googleapis.com/drive/v3/files?fields=files(id)&q=trashed=false+and+mimeType='application/vnd.google-apps.$type1'+and+'$parent_id1'+in+parents+and+name='${name1// /+}'"
       resp=$(gLdrv $gurl) || return $?
      unset id
      . <(grep id <<< $resp | sed -r 's/[",]//g;s/: /=/')
      id1=$id
      echo "   id1=$id1"
      parent_id1=$id1
    done <<< "${gdir1////$'\n'}"
        name1=${path1##*/}
       fields="mimeType,id"
         gurl="https://www.googleapis.com/drive/v3/files?fields=files($fields)&q=trashed=false+and+'$parent_id1'+in+parents+and+name='${name1// /+}'"
         resp=$(gLdrv $gurl) || return $?
    unset mimeType id
    . <(grep -E 'mimeType|id' <<< $resp | sed -r 's/[",]//g;s/: /=/')
          id1=$id
    mimeType1=$mimeType
     element1=$([[ $mimeType1 =~ .+folder ]] && echo "  dir1" || echo " file1")
    echo "$element1=$name1"
    echo "   id1=$id1"



    echo



    path2=$(gLrealpath "$2")
    gdir2=${path2%/*}
    gdir2=${gdir2:1}
    type2=folder
    parent_id2=root
    [[ $gdir2 ]] && while read dir2
    do
      echo "  dir2=$dir2"
      name2=$dir2
       gurl="https://www.googleapis.com/drive/v3/files?fields=files(id)&q=trashed=false+and+mimeType='application/vnd.google-apps.$type2'+and+'$parent_id2'+in+parents+and+name='${name2// /+}'"
       resp=$(gLdrv $gurl) || return $?
      unset id
      . <(grep id <<< $resp | sed -r 's/[",]//g;s/: /=/')
      id2=$id
      echo "   id2=$id2"
      parent_id2=$id2
    done <<< "${gdir2////$'\n'}"
        name2=${path2##*/}
       fields="mimeType,id"
         gurl="https://www.googleapis.com/drive/v3/files?fields=files($fields)&q=trashed=false+and+'$parent_id2'+in+parents+and+name='${name2// /+}'"
         resp=$(gLdrv $gurl) || return $?
    unset mimeType id
    . <(grep -E 'mimeType|id' <<< $resp | sed -r 's/[",]//g;s/: /=/')
          id2=$id
    mimeType2=$mimeType
     element2=$([[ $mimeType2 =~ .+folder ]] && echo "  dir2" || echo " file2")
    echo "$element2=$name2"
    echo "   id2=$id2"

    [[ $id1 ]] || { echo 'not found'; return ${gERR[notfound]}; }

    unset name parents
    [[ $element2 =~ file2 ]] &&    name="name: '$name2'"      ||    name="name: '$name1'"
    [[ $id2               ]] && parents="parents: [ '$id2' ]" || parents="parents: [ '$parent_id2' ]"

    prop="
           $name
           $parents
         "
    data=$(gLdata "$prop")

    echo "$data"

    gurl="https://www.googleapis.com/drive/v3/files/$id1/copy"
    resp=$(gLdrv $gurl \
             -X POST \
             -H Content-Type:application/json \
             -d "$data")
    echo "$resp"

  }



  gstat() # retorna estatisticas do arquivo/diretório
  {

    path=$(gLrealpath "$1")
    gdir=${path%/*}
    gdir=${gdir:1}
    type=folder
    parent_id=root
    [[ $gdir ]] && while read dir
    do
    # echo "  dir=$dir"
      name=$dir
       gurl="https://www.googleapis.com/drive/v3/files?fields=files(id)&q=trashed=false+and+mimeType='application/vnd.google-apps.$type'+and+'$parent_id'+in+parents+and+name='${name// /+}'"
       resp=$(gLdrv $gurl) || return $?
      unset id
      . <(grep id <<< $resp | sed -r 's/[",]//g;s/: /=/')
    # echo "   id=$id"
      parent_id=$id
    done <<< "${gdir////$'\n'}"
        name=${path##*/}
       fields="mimeType,id"
         gurl="https://www.googleapis.com/drive/v3/files?fields=files($fields)&q=trashed=false+and+'$parent_id'+in+parents+and+name='${name// /+}'"
         resp=$(gLdrv $gurl) || return $?
    unset mimeType id
    . <(grep -E 'mimeType|id' <<< $resp | sed -r 's/[",]//g;s/: /=/')
    mimeType=$mimeType
     element=$([[ $mimeType =~ .+folder ]] && echo "  dir" || echo " file")
  # echo "$element=$name"
  # echo "   id=$id"

    [[ $id ]] || { echo 'not found'; return ${gERR[notfound]}; }

  # fields="*"
    fields="mimeType,modifiedTime,name,shared,trashed,isAppAuthorized,id,permissionIds"
      gurl="https://www.googleapis.com/drive/v3/files/$id?fields=$fields"
      resp=$(gLdrv $gurl)
    echo "$resp"

  }



_______TODO()(:) # >>------------------------------------------------------------------------------<< #



  gfind() # pesquisa por nome na hierarquia de diretórios
  {

    # listagem de todos os metadados de todos os elementos em parent id fora da lixeira
      gLdrv "https://www.googleapis.com/drive/v2/files" | less
      gLdrv "https://www.googleapis.com/drive/v3/files"

      gLdrv "https://www.googleapis.com/drive/v3/files?q=trashed=false&fields=files(name,id,parents)&orderBy=name"
      # i:0AMxqLcLzZryZUk9PVA
      # n:<root>
      # |
      # | p:0AMxqLcLzZryZUk9PVA
      # | i:1Ho8w13dvt89o0lgos_0ANz5GujLV3fUm6Jh2cHBxc7c
      # | n:file1
      # |
      # | p:0AMxqLcLzZryZUk9PVA
      # | i:1j4h9zg5aFmGWSbgDdp3WscASl5c-blHb
      # | n:dir1
      #   |
      #   | p:1j4h9zg5aFmGWSbgDdp3WscASl5c-blHb
      #   | i:1DeW-TMl_DiX_P3mGe1rMEjZOzOKqO340v68CV1jPxj8
      #   | n:file2
      #   |
      #   | p:1j4h9zg5aFmGWSbgDdp3WscASl5c-blHb
      #   | i:1Go1ECltn4lOwN2e5mhZom9t6YxwESpha
      #   | n:dir2
      #     |
      #     | p:1Go1ECltn4lOwN2e5mhZom9t6YxwESpha
      #     | i:1yuFQqmu2M6BaOb36TfQOgIWClNbPQiYMjmtyzP_WIak
      #     | n:file3
      #     |
      #     | p:1Go1ECltn4lOwN2e5mhZom9t6YxwESpha
      #     | i:1kBbbyczPQfElnMcGJNckDPDgNFZYhNcX
      #     | n:dir3
      #       |
      #       | p:1kBbbyczPQfElnMcGJNckDPDgNFZYhNcX
      #       | i:14Otma4r8P6FvIipilIm6_qWbDs36uAobGdC9L4qlQGw
      #       | n:file4

      gLdrv "https://www.googleapis.com/drive/v3/files/1DeW-TMl_DiX_P3mGe1rMEjZOzOKqO340v68CV1jPxj8"

      # OU

      query="'$gdir'+in+parents+and+trashed=false"
      fields="*"

    # pesquisa por nome

      name=pasta
      type=folder

      name=ex.svl
      type=document

      query="'$gdir'+in+parents+and+trashed=false+and+mimeType='application/vnd.google-apps.$type'+and+name='$name'"
    # query="'$gdir'+in+parents+and+trashed=false+and+mimeType='application/vnd.google-apps.$type'"
      fields="name,id,mimeType,size,quotaBytesUsed,modifiedTime"

    gurl="https://www.googleapis.com/drive/v3/files?q=$query&fields=files($fields)&orderBy=name"
    resp=$(gLdrv $gurl)
    echo "$resp"

  }



  gtouch() # cria arquivo vazio / atualiza data de modificação de arquivo
  {
    :
  }



  gwho() # retorna quem está acessando o arquivo
  {
    :
  }



  gwhoami() # retorna o username da conta google conectada
  {
    :
  }



  gget() # baixa arquivo do google drive
  {
    :
  }



  gput() # armazena arquivo no google drive
  {
    :
  }



  gpurge() # esvazia lixeira
  {

    gurl="https://www.googleapis.com/drive/v3/files/trash"
    resp=$(gLdrv $gurl -X DELETE)
    echo "$resp"
    # {
    #  "error": {
    #   "errors": [
    #    {
    #     "domain": "global",
    #     "reason": "insufficientPermissions",
    #     "message": "Insufficient Permission"
    #    }
    #   ],
    #   "code": 403,
    #   "message": "Insufficient Permission"
    #  }
    # }

  }



_______INIT()(:) # >>------------------------------------------------------------------------------<< #



  for c in b g r w y; { ac ${c}1kd ''; } # seta as variáveis das cores principais



  lib_name=${y1kd}gAPI.sh${off}

 # <http://unicode.org/emoji/charts/full-emoji-list.html>
  happy_ico=🙂 # ☺
  sorry_ico=🙁 # ☹
  cloud_ico=🌧 # ☁ 🌨🌩🌩⛅
    doc_ico=📄
    dir_ico=📁
  trash_ico=🗑
   time_ico=⏳
   link_ico=🔗
  token_ico=🏷
  check_ico=✅✔️
   more_ico=🕸🕷🐧🐢

  typeset -gA gERR
  gERR=(
         [noclient]=1
           [noauth]=2
          [expired]=3
          [nocloud]=4
         [notfound]=5
            [other]=6
             [help]=255
       )

      gTTY=$( ( tty || echo unk ) | cut -d/ -f3- )
  gLWDFILE="${gLWDFILE:-$HOME/.gapi-lwd}"

  [[ $(type -t gapi) == function ]] && exists=true || exists=false

  $exists && pref=atualiz || pref=carreg
  $exists && suf1=e       || suf1=ue
  $exists && suf2=ada     || suf2=ada
  $exists &&   or="\nou\n  ${y1kd}gapi ${g1kd}u${off}${y1kd}pdate${off}"



  if [[ $0 =~ -?bash ]]
  then
    echo -e "Biblioteca $lib_name $pref$suf2"
    gLcomp
    if [[ ! $FUNCNAME ]]
    then
      [[ $c_name ]] || gapi c curl #| sed '1d;$d'
      gLstatusupd
      (( gapi[$c_name-token_expiring_in] < 300 )) && gapi a
      gsp
      gcd :
    fi
  else
    echo -e "${pref^}$suf1 a biblioteca $lib_name através de source:\n  $g1kd. $0$off$or"
  fi



# EOF
