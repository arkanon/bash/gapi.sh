# Google API BASh CLI

## Uso

1. [Primeira vez](#primeira-vez-)
1. [Outras vezes](#outras-vezes-)

### Primeira vez [§](#uso)

1. logar com uma conta do google <https://accounts.google.com>

1. acessar a console do desenvolvedor <https://console.developers.google.com>

1. aceitar os termos de serviço
   ```
   marcar o checkbox
   selecionar o pais de residência
   aceitar
   ```

1. criar um projeto (até 12)
   ```
   nome
   código do projeto
   localização: sem organização
   criar
   ```

1. ativar as apis desejadas no projeto selecionado <https://console.developers.google.com/apis/library>
   ```
   api do google drive <https://console.developers.google.com/apis/library/drive.googleapis.com>
   ativar
   ```

1. definir o nome do aplicativo na tela de consentimento do oauth
   ```
   <https://console.developers.google.com/apis/credentials/consent>
   nome do aplicativo
   logotipo do aplicativo
   e-mail para suporte
   escopos das apis do google
     adicionar escopo
       Google Drive API   ../auth/drive.file   ver e gerenciar os arquivos e pastas do Google Drive que você abriu ou criou com este aplicativo
       adicionar
   salvar
   ```

1. criar uma credencial oauth (id de cliente) para o curl
   ```
   <https://console.developers.google.com/apis/credentials>
   menu credenciais
     aba credenciais
       criar credenciais
         id do cliente oauth
           tipo de aplicativo: outro
           nome: curl
           criar
   ```

   O OAuth é limitado a 100 logins de escopo sensível antes da publicação da tela de consentimento do OAuth.
   A publicação pode exigir um processo de verificação que leva vários dias.

1. criar o arquivo de configuração da biblioteca gapi.sh

   ```bash
   $ cat << EOT >| ~/.gapi
   # vim: ft=sh

   # ~/.gapi

     [ curl-client_id     ] = 000000000000-00000000000000000000000000000000.apps.googleusercontent.com
     [ curl-client_secret ] = 000000-00000000000000000
     [ curl-scope         ] = https://www.googleapis.com/auth/drive.file+https://www.googleapis.com/auth/docs
                            # <https://developers.google.com/identity/protocols/googlescopes>
                            # ver e gerenciar os arquivos e pastas do Google Drive que você abriu ou criou com este aplicativo
                            # ver, editar, criar e excluir seus documentos do Google Drive

   # EOF
   EOT
   ```

### Outras vezes [§](#uso)

1. importar no bash a biblioteca gapi.sh

   ```bash
   $ . gapi.sh
   Funções gapi definidas
   Cliente curl selecionado
   [6] atualizar o access_token usando o refresh_token [1/v-00000000000000000000000000000000000000000]
       O token de acesso expira em 2019/03/14 Thu 02:08:56 -03
   [7] para revogar a autorização:
       gapi revoke
   ```

1. gerenciar o token

   ```bash
   $ gapi

   Uso: gapi <ação>

   Ações:

        list               lista os clientes configurados em ~/.gapi
        client <cliente>   seleciona o cliente a ser usado
        auth               inicializa ou atualiza a autorização do cliente selecionado
        revoke             revoga a autorização do cliente selecionado
        status             exibe os dados do cliente selecionado
        forget             remove a biblioteca gAPI
        verbose            ativa o modo debug
        quiet              desativa o modo debug
        update             recarrega as definições das funções de acesso à API

   Biblioteca:

        gapi      manage tokens and library
        gsp       set prompt
        gplwd     print last working directory per tty
        gpwd      print working directory
        gcd       change directory
        gls       list directory content
        gmkdir    make directory hierarchy
        gshare    share file/directory
        gtrash    send/recover file/directory to/from trash
        grm       definitly remove file/directory
        gmkfile   make file
        gcat      dump file content
        gupdate   update file
        gmv       move/rename file/directory
        gcp       copy file/directory

   Cliente selecionado: curl
   Token expira em 2019/03/15 Fri 10:44:12 -03 (mais 3593s)

   Credenciais de autenticação dos clientes: ~/.gapi
   Estado      de autorização  dos clientes: ~/.gapi-*

   Debug DESATIVADO
   ```

```
# EOF
```
